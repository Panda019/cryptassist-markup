const mix = require('laravel-mix');

mix.setPublicPath('dist')
    .js('src/js/app.js', 'dist/assets/js/app.js')
    .sass('src/scss/app.scss', 'dist/assets/css/app.css')
    .options({
        processCssUrls: false
    })
    .disableNotifications()
    .browserSync({
        proxy: 'my-domain.test',
        files: [
            'dist/**/*.+(html)',  
            'dist/assets/js/app.js',
            'dist/assets/css/app.css'
        ]
    });